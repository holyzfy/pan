app.dialogConfirm = function (message, callback) {
    var options = {
        template: '#confirm-tmpl',
        data: function () {
            return {
                message: message
            };
        },
        methods: {
            submit: function () {
                callback && callback(/*可以在这里把参数可以传给回调*/);
                this.close();
            },
            close: function () {
                this.message = '';
            }
        }
    };
    var Dialog = Vue.extend(options);
    var instance = new Dialog({
        el: document.createElement('div')
    });
    $('body').append(instance.$el);
};