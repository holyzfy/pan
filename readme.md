# 育网云

## 使用说明

- 请在web服务器里查看`login.html`，服务器需开启[SSI](https://en.wikipedia.org/wiki/Server_Side_Includes)

## 开发文档

- [表单校验示例](http://codepen.io/holyzfy/pen/xggPNL)，[文档](https://jqueryvalidation.org/documentation/)
- [jstree](https://github.com/vakata/jstree)