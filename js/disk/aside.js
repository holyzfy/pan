(function () {

var data = {
    shortcutNav: app.shortcutNav,
    moreSelected: false,
    diskType: ''
};

var tree = {
    inserted: function (el) {
        var options = {
            core: {
                multiple: false
            }
        };
        $(el).jstree(options);
    }
};

function toggleDiskType(type) {
    this.diskType = this.diskType === type ? '' : type;
}

var side = new Vue({
    el: '#aside',
    data: data,
    directives: {
        tree: tree
    },
    methods: {
        toggleDiskType: toggleDiskType
    }
});

})();